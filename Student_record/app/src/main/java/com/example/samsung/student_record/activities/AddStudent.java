package com.example.samsung.student_record.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.samsung.student_record.R;

public class AddStudent extends ActionBarActivity {

    Button saveButton, cancelButton;
    EditText userName, userRollNo, userEmail, userContact;
    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity2);
        userName = (EditText) findViewById(R.id.e1);
        userRollNo = (EditText) findViewById(R.id.e2);
        userEmail = (EditText) findViewById(R.id.email_student);
        userContact = (EditText) findViewById(R.id.contact_student);
        saveButton = (Button) findViewById(R.id.b1);
        cancelButton = (Button) findViewById(R.id.b2);
        extras = getIntent().getExtras();
        if (extras != null) {                     //DONE
            if (extras.get("Button").equals("view")) {
                String name = (String) extras.get("Name");
                userName.setText(name);
                String rollNo = (String) extras.get("RollNo");
                userRollNo.setText(rollNo);
                String email = (String) extras.get("Email");
                userEmail.setText(email);
                Integer contact = (Integer) extras.get("Contact");
                userContact.setText(String.valueOf(contact));
                userName.setEnabled(false);
                userRollNo.setEnabled(false);
                userEmail.setEnabled(false);
                userContact.setEnabled(false);
                saveButton.setVisibility(View.INVISIBLE);
                cancelButton.setText("EXIT");
            }
            if (extras.get("Button").equals("edit")) {
                String nameUser = (String) extras.get("Name");
                userName.setHint(nameUser);
                userName.setEnabled(true);
                String rollNoUser = (String) extras.get("RollNo");
                userRollNo.setHint(rollNoUser);
                userRollNo.setEnabled(true);
                String emailUser = (String) extras.get("Name");
                userName.setHint(emailUser);
                userEmail.setEnabled(true);
                String contactUser = (String) extras.get("RollNo");
                userRollNo.setHint(String.valueOf(contactUser));
                userContact.setEnabled(true);
                saveButton.setText("UPDATE DETAILS");
            }
        }
    }

    public void saveDataButton() {
        Intent intent = new Intent(AddStudent.this, StartActivity.class);
        if (userName.equals(null) || userRollNo.equals(null) || userEmail.equals(null) || userContact.equals(null)) {
            Toast.makeText(AddStudent.this, "Please fill all the fields", Toast.LENGTH_SHORT).show();
            intent.putExtra("name", userName.getText().toString());
            intent.putExtra("roll_no", userRollNo.getText().toString());
            intent.putExtra("email", userEmail.getText().toString());
            intent.putExtra("contact", userContact.getText().toString());
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.b1:
                saveDataButton();
                break;
            case R.id.b2:
                finish();
            default:
                break;
        }
    }
}




