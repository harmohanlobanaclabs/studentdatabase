package com.example.samsung.student_record.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.samsung.student_record.R;
import com.example.samsung.student_record.entities.Changes;
import com.example.samsung.student_record.util.Constants;


public class Login extends ActionBarActivity implements Constants {
    Button b1, b2;
    SharedPreferences preferencesObject;
    EditText e1, e2;
    SharedPreferences.Editor editPreferences;
    String username, password;
    Changes.DbHelper DbHelperObject;
    SharedPreferences sharedPreferencesObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        e1 = (EditText) findViewById(R.id.e1);
        e2 = (EditText) findViewById(R.id.e2);
        b1 = (Button) findViewById(R.id.b1);
        b2 = (Button) findViewById(R.id.b2);
        sharedPreferencesObject = getSharedPreferences("Login Credentials", MODE_PRIVATE);
        editPreferences = sharedPreferencesObject.edit();
        editPreferences.putString("Username", "hello");
        editPreferences.putString("Password", "user");
        editPreferences.commit();
    }

    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.b1:
                username = e1.getText().toString();
                password = e2.getText().toString();
                if (username.equals(sharedPreferencesObject.getString("Username", null)) && password.equals(sharedPreferencesObject.getString("Password", null))) {
                    Toast.makeText(this, "Welcome User", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Login.this, StartActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Wrong username or password", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.b2:
                finish();
            default:
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
