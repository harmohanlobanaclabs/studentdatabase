package com.example.samsung.student_record.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.samsung.student_record.R;
import com.example.samsung.student_record.entities.Adapters;
import com.example.samsung.student_record.entities.Changes;
import com.example.samsung.student_record.entities.Student;
import com.example.samsung.student_record.util.Constants;

import java.util.ArrayList;


public class StartActivity extends ActionBarActivity implements Constants {
    Button studentButton;
    AddStudent mainActivity2Object;
    ListView list;
    Adapters adapter;
    Button viewButton, editButton, deleteButton;
    ArrayList<Student> data = new ArrayList<Student>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        studentButton = (Button) findViewById(R.id.addStudent);
        list = (ListView) findViewById(R.id.l1);
        data = Changes.viewAll();
        adapter = new Adapters(this, data);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                Toast.makeText(StartActivity.this, "you clicked ",
                        Toast.LENGTH_SHORT).show();
                final Dialog dialog = new Dialog(StartActivity.this);
                dialog.setContentView(R.layout.activity_dialog__box);
                dialog.setTitle("Please select one : ");
                viewButton = (Button) dialog.findViewById(R.id.view_button);
                editButton = (Button) dialog.findViewById(R.id.edit_button);
                deleteButton = (Button) dialog.findViewById(R.id.delete_button);
                viewButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent viewIntent = new Intent(StartActivity.this, AddStudent.class);
                        String name = data.get(position).name;
                        String rollNo = data.get(position).rollNo;
                        String email = data.get(position).email;
                        Integer contact = data.get(position).contact;
                        viewIntent.putExtra("Name", name);
                        viewIntent.putExtra("RollNo", rollNo);
                        viewIntent.putExtra("Email", email);
                        viewIntent.putExtra("Contact", contact);
                        viewIntent.putExtra("Button", "view");
                        startActivity(viewIntent);
                        dialog.dismiss();
                    }
                });
                editButton.setOnClickListener(new View.OnClickListener() {    // for editing
                    @Override
                    public void onClick(View v) {
                        Intent editIntent = new Intent(StartActivity.this, AddStudent.class);
                        String name = data.get(position).name;
                        String rollNo = data.get(position).rollNo;
                        String email = data.get(position).email;
                        Integer contact = data.get(position).contact;
                        editIntent.putExtra("Name", name);
                        editIntent.putExtra("RollNo", rollNo);
                        editIntent.putExtra("Email", email);
                        editIntent.putExtra("Contact", contact);
                        editIntent.putExtra("Button", "view");
                        startActivityForResult(editIntent, EDIT_REQUEST);
                        dialog.dismiss();
                    }
                });
                deleteButton.setOnClickListener(new View.OnClickListener() {   // for deleting
                    @Override
                    public void onClick(View v) {
                        Changes Dbobject = new Changes(StartActivity.this);
                        Dbobject.open();
                        Dbobject.deleteStudent(mainActivity2Object.userName);
                        list.removeViewAt(position);
                        adapter.notifyDataSetChanged();
                        Dbobject.close();
                        dialog.dismiss();
                    }
                });
                dialog.show();
                setFinishOnTouchOutside(true);
            }
        });
    }

    public void addStudentButton() {
        Intent intent = new Intent(StartActivity.this, AddStudent.class);
        startActivityForResult(intent, ADD_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_REQUEST) {
            if (resultCode == RESULT_OK) {
                boolean success = true;
                try {
                    Changes Dbobject = new Changes(StartActivity.this);
                    Dbobject.open();
                    String name = data.getStringExtra("name");
                    String roll_no = data.getStringExtra("roll_no");
                    String email = data.getStringExtra("email");
                    Integer contact = Integer.valueOf(data.getStringExtra("contact"));
                    Dbobject.createDb(name, roll_no, email, contact);
                    adapter.data.add(new Student(name, roll_no, email, contact));
                    adapter.notifyDataSetChanged();
                    Dbobject.close();
                } catch (Exception e) {
                    success = false;
                    e.getMessage();
                } finally {
                    if (success) {
                        Dialog d = new Dialog(StartActivity.this);
                        d.setTitle("Record Successfully Added");
                    }

                }
            } else if (requestCode == EDIT_REQUEST) {
                if (resultCode == RESULT_OK) {
                    boolean success = true;
                    try {
                        Changes Dbobject = new Changes(StartActivity.this);
                        Dbobject.open();
                        String editName = data.getStringExtra("name");
                        String editRoll_no = data.getStringExtra("roll_no");
                        String editEmail = data.getStringExtra("email");
                        Integer editContact = Integer.valueOf(data.getStringExtra("contact"));
                        Dbobject.updateDb(editName, editRoll_no, editEmail, editContact);
                        adapter.data.add(new Student(editName, editRoll_no, editEmail, editContact));
                        adapter.notifyDataSetChanged();
                        Dbobject.close();
                    } catch (Exception e) {
                        success = false;
                        e.getMessage();
                    } finally {
                        if (success) {
                            Dialog d = new Dialog(StartActivity.this);
                            d.setTitle("Database Successfully Updated");
                        }
                    }
                }
            }
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.addStudent:
                addStudentButton();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
