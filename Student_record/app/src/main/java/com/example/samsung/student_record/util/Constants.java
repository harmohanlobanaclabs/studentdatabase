package com.example.samsung.student_record.util;

/**
 * Created by samsung on 2/4/2015.
 */
public interface Constants {
    public static final String STUDENT_ROLLNO = "student_rollNo";
    public static final String STUDENT_NAME = "student_name";
    public static final String STUDENT_EMAIL = "student_email";
    public static final String DATABASE_NAME = "Student_database";
    public static final String DATABASE_TABLE = "Student_table";
    public static final int DATABASE_VERSION = 1;
    public static final String STUDENT_CONTACT = "student_contact";
    int ADD_REQUEST = 5;
    int EDIT_REQUEST = 6;
}
