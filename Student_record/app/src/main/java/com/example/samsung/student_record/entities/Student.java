package com.example.samsung.student_record.entities;

/**
 * Created by samsung on 1/22/2015.u
 */
public class Student {
    public String name;
    public String rollNo;
    public String email;
    public Integer contact;

    public Student(String name, String roll_no, String email, Integer contact) {
        this.name = name;
        this.rollNo = roll_no;
        this.email = email;
        this.contact = contact;
    }
}



