package com.example.samsung.student_record.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.ActionBarActivity;
import android.widget.EditText;

import com.example.samsung.student_record.util.Constants;

import java.util.ArrayList;

/**
 * Created by samsung on 2/4/2015.
 */

public class Changes extends ActionBarActivity implements Constants {

    private DbHelper ourHelper;
    private final Context ourContext;
    private static SQLiteDatabase ourDatabase;

    public Changes(Context ourContext) {
        this.ourContext = ourContext;
    }

    public Changes open() {
        ourHelper = new DbHelper(ourContext, DATABASE_NAME, null, DATABASE_VERSION);
        ourDatabase = ourHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        ourHelper.close();
    }

    public long createDb(String name, String roll_no, String email, Integer contact) {
        try {
            ContentValues cvInsert = new ContentValues();
            cvInsert.put(STUDENT_NAME, name);
            cvInsert.put(STUDENT_ROLLNO, roll_no);
            cvInsert.put(STUDENT_EMAIL, email);
            cvInsert.put(STUDENT_CONTACT, contact);
            return ourDatabase.insert("Student_table", null, cvInsert);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -2;
    }

    public void updateDb(String editName, String editRoll_no, String editEmail, Integer editContact) {
        ContentValues cvUpdate = new ContentValues();
        cvUpdate.put(STUDENT_NAME, editName);
        cvUpdate.put(STUDENT_ROLLNO, editRoll_no);
        cvUpdate.put(STUDENT_EMAIL, editEmail);
        cvUpdate.put(STUDENT_CONTACT, editContact);
        ourDatabase.update(DATABASE_TABLE, cvUpdate, STUDENT_NAME + " = " + editName, null);
    }


    public ArrayList viewAll() {

        ArrayList<Student> studentList = new ArrayList<>();
        String[] columns = new String[]{STUDENT_NAME, STUDENT_ROLLNO, STUDENT_EMAIL, STUDENT_CONTACT};
        Cursor c = ourDatabase.query(DATABASE_TABLE, columns, null, null, null, null, null);
        String name = c.getString(c.getColumnIndex(STUDENT_NAME));
        String rollNo = c.getString(c.getColumnIndex(STUDENT_ROLLNO));
        String email = c.getString(c.getColumnIndex(STUDENT_EMAIL));
        int contact = c.getInt(c.getColumnIndex(STUDENT_CONTACT));
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            studentList.add(new Student(name, rollNo, email, contact));
        }
        return studentList;
    }

    public void deleteStudent(EditText userName) {
        {
            ourDatabase.delete(DATABASE_TABLE, STUDENT_NAME + "=" + userName, null);
        }

    }

    public static class DbHelper extends SQLiteOpenHelper implements Constants {
        public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, DATABASE_NAME, factory, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + DATABASE_TABLE + "(" + STUDENT_NAME + " STRING PRIMARY KEY, " + STUDENT_ROLLNO + " TEXT NOT NULL , " + STUDENT_EMAIL + " TEXT NOT NULL, " + STUDENT_CONTACT + " INTEGER NOT NULL); ");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("TABLE ALREADY EXIST " + DATABASE_NAME);
            onCreate(db);
        }
    }
}
